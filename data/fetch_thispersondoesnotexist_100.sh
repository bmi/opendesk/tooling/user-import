# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
#!/bin/bash

# as we experienced images fetched multiple times we name them after their md5 checksum to avoid doublettes.
mkdir -p images_request
cd images_request
for i in {1..100}
do
   curl https://thispersondoesnotexist.com/ --output temporary_filename.jpg
   mv -f temporary_filename.jpg $( md5sum temporary_filename.jpg  | cut -c 1-32 ).jpg
done
