# [3.3.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v3.2.0...v3.3.0) (2025-02-07)


### Bug Fixes

* Update `curl` pinning to v8.12.0-r0 ([e421940](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/e421940c073f7e4e29a76a943f01e50115b6645c))


### Features

* Support for `localhost_port` when working with K8s tunnels ([66e1435](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/66e14356b1b6bc060fb502c88374c33db3ac944e))

# [3.2.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v3.1.1...v3.2.0) (2025-01-14)


### Features

* Options to make groups accessible to fileshare, knowledge management and project management ([7e5de51](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/7e5de511c27301910ed7c309bf008a5f5bd4a20a))

## [3.1.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v3.1.0...v3.1.1) (2024-12-18)


### Bug Fixes

* Handle race condition when two scripts in parallel check for groups ([1e847a7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/1e847a7c5c08acad3faefb22667014f935a18c8a))

# [3.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v3.0.0...v3.1.0) (2024-12-18)


### Bug Fixes

* Bump CI to 2.4.8 ([c339851](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/c3398511fd6064bb4874623f2b88e5dcdfe0ec57))
* **dockerfile:** Bump Python and Curl packages ([0b941b0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/0b941b0843b017cf102d673af22844318e32cae3))


### Features

* Add support for Notes app ([0fbcc7b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/0fbcc7b2ca0fc237185d5355db14d6f2d38e5053))

# [3.0.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v2.3.1...v3.0.0) (2024-12-05)


### Features

* CLI parameter `--password_recovery_email` overrules defined addresses of the same purpose when importing a list of users. ([007bf48](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/007bf482f1d3039806d4952c44859f78ac4769f9))


### BREAKING CHANGES

* If you import user lists please ensure you do not set the option for `password_recovery_email` on CLI or via environment variable, unless you want that email address to overwrite what is defined in your import list.

## [2.3.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v2.3.0...v2.3.1) (2024-11-27)


### Bug Fixes

* Do not ignore invitation mail setting ([f8aca97](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/f8aca9742bb64cd5ac6ba63fb6b1bd3edf395be8))

# [2.3.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v2.2.0...v2.3.0) (2024-11-27)


### Features

* Support --verify_certificate option for private CA dev scenario ([43bd43c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/43bd43c9349e3795f1cd830d0c478cc6f7fcf227))

# [2.2.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v2.1.2...v2.2.0) (2024-11-22)


### Features

* Add curl to dockerfile ([df16db4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/df16db41a0e25098c64ddc8416c193319918bfc7))

## [2.1.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v2.1.1...v2.1.2) (2024-11-12)


### Bug Fixes

* Consistent boolean parameter checking ([1f28113](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/1f28113241c5b190acddb528fd9a2724adbc99ea))
* Consistent type checks for cli arguments and ENV variables ([c15b0dd](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/c15b0dd4bfd9c4f36199fefe64aa8435a878adc5))
* **import_user:** Adjusted parameter checks ([2c2b03e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/2c2b03ec43d8fe6295451c805843be7fd9b5a1d1))

## [2.1.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v2.1.0...v2.1.1) (2024-11-06)


### Bug Fixes

* Merge of dicts for Python <3.9. ([fe0558e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/fe0558ed00563a97284de121c5c134f16c7b0964))
* Support numbers only on import of strings like lastname and firstname. ([0a6eebe](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/0a6eebe4078dcaf960e43de4405c4349b2ec6c60))

# [2.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v2.0.2...v2.1.0) (2024-10-12)


### Bug Fixes

* Bump Alpine base image to 3.20.3 and Python. ([8e897d3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/8e897d3eba7b95ecf42bfadec41a1683b949876f))
* Bump gitlab-config to 2.4.2 ([037c413](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/037c41382b527037615a348be8ec8a7d587efb50))
* REUSE.software compliance. ([df53b1c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/df53b1c47ec1cccf5a92c26f031011f9a622884f))


### Features

* Check and filter out non available user attributes. ([82d2504](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/82d2504e27aaef502cf5ef5dccf976f7922021c9))

## [2.0.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v2.0.1...v2.0.2) (2024-10-02)


### Bug Fixes

* Random_usernames default setting. ([5f9709a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/5f9709a7f877b21459619a2283442bf0f1fd97ae))

## [2.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v2.0.0...v2.0.1) (2024-10-01)


### Bug Fixes

* Add all files for random import to container image. ([a2cd52d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/a2cd52dfd1cf5663805ed2fbbc5940fce9da7e1c))

# [2.0.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.8.0...v2.0.0) (2024-10-01)


### Features

* Support for schematic usernames and default `udm_api_username` to "Administrator". ([f2c00af](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/f2c00afacd0fc1e834be55208116d0e3b50d40ab))


### BREAKING CHANGES

* Default import username has changed.

# [1.8.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.7.4...v1.8.0) (2024-09-27)


### Features

* Added option to upload random profile pictures when importing users from a file. ([d708be7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/d708be7478d9a65775d89c6498c4c148e3bda096))

## [1.7.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.7.3...v1.7.4) (2024-09-26)


### Bug Fixes

* Add `--admin_enable_knowledgemanagement` option with backwards compatibility. ([923f618](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/923f6181c123c317ebb55a68d9fec6b53ff9fe95))

## [1.7.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.7.2...v1.7.3) (2024-09-26)


### Bug Fixes

* Add `--admin_enable_knowledgemanagement` option (in development in openDesk). ([fdd1bc7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/fdd1bc717afd74797aff5ebcecfece46c605ebc7))

## [1.7.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.7.1...v1.7.2) (2024-09-26)


### Bug Fixes

* Add `--admin_enable_knowledgemanagement` option (in development in openDesk). ([07e11cf](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/07e11cf2edf83a34698a7645b6a0f5303af5bdc6))

## [1.7.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.7.0...v1.7.1) (2024-09-23)


### Bug Fixes

* Print summary of created account not from log file. ([42f7086](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/42f70862e26ce69363d57e8de2e970b9db91eb99))

# [1.7.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.6.1...v1.7.0) (2024-09-19)


### Features

* Support for `--output_accounts_filename` to set the filename the created accounts are written to. ([be1604e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/be1604ec69b575d7c4216bfdb0594753be4d451f))

## [1.6.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.6.0...v1.6.1) (2024-09-19)


### Bug Fixes

* Improve import filehandling and log output. ([551e9e0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/551e9e0ddf11b0faa2b4360bcbd41c04a85e256f))
* Print out settings of the run at the beginning of the script. ([03b61e7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/03b61e7f9730e4baeeb60855e149daff7560b45f))

# [1.6.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.5.1...v1.6.0) (2024-09-13)


### Bug Fixes

* **docker:** Bump Python to 3.12.6-r0. ([d9acf80](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/d9acf80020357fcf4ba5650e8c767e7450fb8480))


### Features

* Support for OXContexts incl. autocreation. ([262d050](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/262d050923b90f483ae859917e9165b0c0363d8d))

## [1.5.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.5.0...v1.5.1) (2024-09-06)


### Bug Fixes

* Improve logging and attribute cleanup. ([7d87cbf](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/7d87cbf675bb7113bce87ff1a745b48e0b67f2fa))

# [1.5.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.4.3...v1.5.0) (2024-09-04)


### Bug Fixes

* string.strip group names. ([dec9914](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/dec991415c530f0275a58bcf2ba39219a4d51512))


### Features

* Print create stats. ([baa8294](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/baa8294cadf26949559f655a134bacf15b4d329c))

## [1.4.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.4.2...v1.4.3) (2024-09-04)


### Bug Fixes

* Use pip to install Python dependencies. ([27f2b63](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/27f2b63bf4297bea6b90d62612313d36e0d7d528))
* Use pip to install Python dependencies. ([91a05dc](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/91a05dc30dab1696fadacfdae5384fb7c5225ded))

## [1.4.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.4.1...v1.4.2) (2024-09-04)


### Bug Fixes

* Add template to Docker image. ([e27885e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/e27885e4309bcbbee2ddf73d7ce76a6018d318f5))

## [1.4.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.4.0...v1.4.1) (2024-09-04)


### Bug Fixes

* Default Debug level is INFO. ([120e104](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/120e104d52322dfac8804be94918cc4c983f5b34))

# [1.4.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.3.2...v1.4.0) (2024-09-02)


### Features

* `--trigger_invitation_mail True` triggers password reset emails after a new user was created. ([1b7cdd4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/1b7cdd48051886013ecb4147f75b98b7da8fe2fb))

## [1.3.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.3.1...v1.3.2) (2024-08-29)


### Bug Fixes

* **docker:** Update alpine and python. ([a4908f0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/a4908f00c5d559d5908ae02ff7dd60075695e31f))
* **groups:** Handle no groups in import lists properly ([254bcb8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/254bcb8dc2b7e2861687297e3798e6981682219c))

## [1.3.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.3.0...v1.3.1) (2024-08-22)


### Bug Fixes

* **list_import:** Skip lines where email address is `0` and append created maildomains to internal lookup list. ([daee57b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/daee57b9ff8b5def9f032111dd6a70c78d90de89))

# [1.3.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.2.0...v1.3.0) (2024-08-22)


### Features

* Auto-create maildomain option for list imports. ([830e44f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/830e44f6cce2c1a4e7c725f83eeb9809e108e25c))

# [1.2.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.1.0...v1.2.0) (2024-08-22)


### Features

* Support to set the openDesk flags (application access and admin) on commandline. ([04e4184](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/04e418411009f1bb71e6cb3dd2229bee26e2a50b))

# [1.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/compare/v1.0.0...v1.1.0) (2024-07-01)


### Features

* Support for group reconciliation. ([d07a95d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/d07a95d8d138afa567b1a8dc8b6f9e7ebac34172))

# 1.0.0 (2024-06-28)


### Bug Fixes

* Allow no-group users as well. ([e2fd409](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/e2fd409646c1ac9aeaf62ab9760d7f44d54e0f16))
* Also print entryuuid in log output. ([5e3e929](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/5e3e9290c217f15aae5b64c91934b4acf097b093))
* Import title ([c805a15](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/c805a158e7c88feee08ca197eb2dd8e90cb39ac2))
* Pass CLI attribute for password_recovery_email to random user ([ea783f1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/ea783f18ea35dc3d8942c6db53305ee2855ecb1b))
* Print credentials when importing random users ([050fe6a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/050fe6ab22b3d0d87136d2f92180e6e2342f78b2))
* **requirements.txt:** Add configargparse ([bbe9566](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/bbe95666f2e104343b5deff6d604949352e207aa))
* Support for separate mail domain ([d70878e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/d70878e7ef0d87031f363056e68c1c9db22a17d7))
* Typo and shebang ([27594e1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/27594e1b1d9aaff5dd6aabba1f77d84f8e10b87b))
* Update template.ods ([040e0ba](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/040e0ba536de98572c325ab1f98e67f6d2176b82))
* Use correct request body when logging errors of group creation ([0945726](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/09457261d16143bcb471e8714ff7a3f1d8ffcad9))
* **validation:** Proper message when username is missing. ([ef53022](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/ef530227fd60035d742b17824480ec46050f3bcc))


### Features

* Add groups ([5ab6de5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/5ab6de5594d818900916938d34fe3f032ad12696))
* Add organisation for list import, fix bugs. ([11721a5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/11721a55fd669c47c35cc849d91767a94f200a38))
* Allow "Passwort" column on ods based user import ([4065a3c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/4065a3cb43734eb4adab63f7010d62e8759a2715))
* **ci:** Add CI option ([1c94857](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/1c94857dbcde58facd06fd84c193fe5160dce73c))
* **ci:** Always show counter ([4d212a3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/4d212a3967e60c1c164184d032d9ca634330c9e3))
* **ci:** Improve log output ([bdb4fbd](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/bdb4fbd0c1d78b4cdbfff862279908e1d4d027b6))
* **ci:** Refine CI option ([263d68c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/263d68c469caaf893b7a245fb296092782e4c338))
* **ci:** Refine CI option ([0123a57](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/0123a57a1dbe9853c8e17a086e55754bb49c3e0d))
* **ci:** Refine CI option ([5106d64](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/5106d64287db02fa11d06967902622702e89d823))
* **ci:** Refine CI option ([67218af](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/67218afa737cf89f29d31309c482f0211ae32ac0))
* **ci:** Refine CI option ([00f0283](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/00f0283d772b67cd5e4e0e265417fa6a67877325))
* Docker image. ([3d4517c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import/commit/3d4517c8fdc8a6ea0e1afda439cb549ac85d5ba5))
