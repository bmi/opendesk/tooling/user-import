# SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-License-Identifier: Apache-2.0

FROM registry-1.docker.io/library/alpine:3.20.3

RUN apk add --no-cache \
    python3=3.12.8-r1 \
    py3-pip=24.0-r2 \
    bash=5.2.26-r0 \
    kubectl=1.30.0-r3 \
    curl=8.12.0-r0 \
  && addgroup -S "app" \
  && adduser -D -G "app" -h "/app" -s "/bin/bash" -u 1000 -S "app"

USER app

WORKDIR /app

COPY ./user_import_udm_rest_api.py /app/
COPY ./requirements.txt /app/
COPY ./template.ods /app/
COPY ./lib/ /app/lib/
COPY ./data/ /app/data/

RUN pip install --no-cache-dir --break-system-packages -r /app/requirements.txt

CMD [ "/app/user_import_udm_rest_api.py"]
