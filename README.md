<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

<h1>openDesk User import or generation tool</h1>

This script allows you to either import an existing user list into an openDesk environment, or to create randomly generated fictitious users for it.

* [Prerequisites](#prerequisites)
  * [Running the script locally](#running-the-script-locally)
  * [Running the script from within the container](#running-the-script-from-within-the-container)
* [Commandline](#commandline)
* [Import modes](#import-modes)
  * [Import defined list of users](#import-defined-list-of-users)
  * [Random demo accounts](#random-demo-accounts)
    * [Demo data (sources)](#demo-data-sources)
      * [Text files](#text-files)
      * [Profile pictures: `/data/images_?/*`](#profile-pictures-dataimages_)
* [License](#license)
* [Copyright](#copyright)

# Prerequisites

- Credentials to use the [UDM REST API](https://docs.software-univention.de/developer-reference/latest/en/udm/rest-api.html), in openDesk the `default.admin` as well as other accounts that are part of the `Domain Admin` group are authorized to make us of the API. This will probably change in the future to use a less generic group to API use only.
- Access to the UDM REST API must be requested by the operator as by default that access is prohibited. In the openDesk deployment the attribute `functional.externalServices.nubus.udmRestApi.enabled` allows to toggle the service.

## Running the script locally

You have to have Python 3 and PIP installed. For Debian based systems you can get it done like this:
```
sudo apt install python3 python3-pip
```

Afterwards you install the script's requirements like:
```
pip install -r requirements.txt
```

## Running the script from within the container

You also can use the image that is build including the script and the necessary prerequisites.

```
export CONTAINER=registry.opencode.de/bmi/opendesk/components/platform-development/images/user-import:latest
docker run --rm -it ${CONTAINER} /bin/sh
```

# Commandline

Execute
```
./user_import_udm_rest_api.py
```

and you will get information which parameters the script requires and/or accepts.

**Note:** No actions will be taken if an user already exists on the import target.

# Import modes

The script can either create random demo accounts or accounts based on a given import list.

## Import defined list of users

The import of a defined list of users can be done based on (./template.ods)[./template.ods].

Some information about the columns:
- `Anrede`: Optional - Salutation / Title.
- `Vorname`: Mandatory - Firstname.
- `Nachname`: Mandatory - Lastname.
- `Externe E-Mail`: Mandatory - External mail address will be set for the self-service e.g. password reset.
- `Username`: Mandatory - The actual login name for the user.
- `LDAP-Gruppen`: Optional - Semicolon separated names of LDAP groups the user should be member of. Script can create these groups if they do not pre-exist.
- `Passwort`: Optional - If no password is specified a complex random password is generated and the user can reset it using the password reset or you can use the `--trigger_invitation_mail` option to let the script trigger that mail during the user creation process.
- `Organisation`: Optional - The value shows just up as the organization of the user. When no value is specified the maildomain from the "Externe E-Mail" is used.
- `Primäre Mailadresse`: Optional - When the groupware module isn't being used you can define an external email address that e.g. receives notifications.
  - Enable the auto creation of non existing maildomains with CLI option `--create_maildomain True`.
- `OX Context`: Optional - When the groupware is used you may want to assign a user a specific [OX Context](https://oxpedia.org/wiki/index.php?title=AppSuite:Context_management) you can define a numeric context in this column.
  - Enable the auto creation of non existing contexts with CLI option `--create_oxcontexts True`.
  - Set the default OX Context `<id>` to use with the CLI option `--default_oxcontext <your_default_context_id>`, if not set it defaults to `1`.

## Random demo accounts

When creating demo accounts the script tries to fill in as much data as possible and also uploads user profile pictures.

### Demo data (sources)

The used data is read from files that can be found in the `./data` directory. There is also a little helper script for fetching additional images.

#### Text files

The sources for the text files are provided in the respective file headers.

#### Profile pictures: `/data/images_?/*`

The profile pictures have been taken from https://thispersondoesnotexists.com which is using [StyleGAN2](https://github.com/NVlabs/stylegan2) to create artificial profile pictures. As copyright protection cannot be applied to AI "art" at the moment, we are free to use the pictures.

To fetch additional pictures you can make use of `fetch_thispersondoesnotexist_100.sh` and sorted the pictures manually into the matching subdirectory.

# License

This project uses the following license: Apache-2.0

# Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
