# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import base64
import glob
import random
import re
import sys
import logging
import pandas as pd

class ImportUser:

    def __init__(self, callback,  import_filename, create_admin_accounts = False, use_images = False, password_recovery_email = None):
        self.input_dir_imgs_base = "./data/images_"
        self.input_file = import_filename
        self.password_recovery_email = password_recovery_email
        self.skip_rows = 0
        self.columnnames_map = {
            'Username': 'username',
            'Externe E-Mail': 'email',
            'Vorname': 'firstname',
            'Nachname': 'lastname',
            'Anrede': 'title',
            'Passwort': 'password',
            'LDAP-Gruppen': 'groups',
            'Organisationseinheit': 'organisation',
            'Primäre Mailadresse': 'mailPrimaryAddress',
            'OX Context': 'oxContext'
        }
        persons = pd.read_excel(self.input_file, engine='odf', skiprows=self.skip_rows)
        persons.rename(columns=self.columnnames_map, inplace=True)

        logging.info(f"Cleaning up list")

        # cleanup list
        error_count = 0
        for index, person in persons.iterrows():
            if person['email'] == 0:
                persons.drop(index, inplace=True)
                continue
            if not isinstance(person['username'], str):
                logging.error(f"Missing username in '{person}'")
                error_count+=1
                continue
            if person['username'].strip() != person['username']:
                logging.warning(f"Leading or trailing blank(s) found in username email: '{person['username']}'")
                persons.at[index, 'username'] = person['username'].strip()
            if person['email'].strip() != person['email']:
                logging.warning(f"Leading or trailing blank(s) found in external email: '{person['email']}'")
                persons.at[index, 'email'] = person['email'].strip()
            if 'mailPrimaryAddress' in person and isinstance(person['mailPrimaryAddress'], str):
                if person['mailPrimaryAddress'].strip() != person['mailPrimaryAddress']:
                    logging.warning(f"Leading or trailing blank(s) found in internal email: '{person['mailPrimaryAddress']}'")
                    persons.at[index, 'mailPrimaryAddress'] = person['mailPrimaryAddress'].strip()

        logging.info(f"Processing list with {len(persons.index)} lines.")
        logging.debug(f"Going to process the following list:\n{persons.to_string()}")

        # validate list
        for index, person in persons.iterrows():
            if not bool(re.match(r'^[\w\d\.-]+$', person['username'], flags=re.IGNORECASE)):
                logging.error(f"Found invalid characters in username: '{person['username']}'")
                error_count+=1
            if not bool(re.match(r'^[\w\d\.\-_]+@[\w\d\.\-_]+$', person['email'], flags=re.IGNORECASE)):
                logging.error(f"Found invalid external email: '{person['email']}'")
                error_count+=1
            if 'mailPrimaryAddress' in person and isinstance(person['mailPrimaryAddress'], str):
                if not bool(re.match(r'^[\w\d\.\-_]+@[\w\d\.\-_]+$', person['mailPrimaryAddress'], flags=re.IGNORECASE)):
                    logging.error(f"Found invalid primary email: '{person['mailPrimaryAddress']}'")
                    error_count+=1
            if 'oxContext' in person and not pd.isna(persons.at[index, 'oxContext']):
                if not isinstance(person['oxContext'], (int, float)):
                    logging.error(f"Invalid oxContext value for user '{person['username']}': {person['oxContext']}. Must be an integer.")
                    error_count += 1

        if (error_count > 0):
            sys.exit("! Found errors, please fix and rerun the script")

        # process list
        for _, person in persons.iterrows():
            if self.password_recovery_email:
                person['email'] = self.password_recovery_email
            # use maildomain of password recovery address if no organisation is explicitly set
            if 'organisation' not in person or pd.isna(person['organisation']):
                person['organisation'] = person['email'].rpartition('@')[-1]
            person['is_admin'] = False
            if use_images:
                person["jpegPhoto"] = self.__get_image()
            callback(person)
            if create_admin_accounts:
                person['username'] = person['username']+'-admin'
                person['is_admin'] = True
                callback(person)

    def __get_image(self):
        if (not hasattr(self, "input_filelist_img_list")):
            self.input_filelist_img_list= []
        if (len(self.input_filelist_img_list) == 0):
            for gender in ("m", "f"):
                self.input_filelist_img_list.extend(glob.glob(self.input_dir_imgs_base+gender+'/*.jpg'))

        selected_image = random.choice(self.input_filelist_img_list)
        with open(selected_image, "rb") as image_file:
            return base64.b64encode(image_file.read()).decode()