# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import random
import unicodedata
import glob
import base64
import random
import string

class RandomUser:

    def __init__(self, callback, create_admin_accounts = False, amount = 100, password_reset_mail = 'not_provided@opendesk.internal', randomize_username=True):
        self.usercounter = 0
        self.randomize_username=randomize_username
        self.input_dir_imgs_base = "./data/images_"
        self.input_files_list = {
            "firstname": "./data/firstname_gender.tsv",
            "lastname": "./data/lastname.txt",
            "organisation": "./data/organisation.txt",
            "city": "./data/city.txt",
            "postcode": "./data/postcode.txt",
            "street": "./data/street.txt",
            "phone": "./data/phone.txt",
            "mobileTelephoneNumber": "./data/mobile.txt"
        }
        self.lists = {}
        for _ in list(range(amount)):
            self.usercounter += 1
            person = {}

            for category in list(self.input_files_list.keys()):
                person[category] = self.__get_random_list_entry(category)

            (person["firstname"], person["gender"]) = self.__get_firstname_and_gender()
            person["username"] = self.__get_username(person["firstname"], person["lastname"])
            person["jpegPhoto"] = self.__get_image(person["gender"])
            person["title"] = self.__get_title(person["gender"])
            person["departmentNumber"] = str(random.randint(1, 50))+"."+str(random.randint(1, 50))+random.choice(string.ascii_lowercase)
            person["roomNumber"] = str(random.randint(1, 50))+"."+str(random.randint(1, 50))+random.choice(string.ascii_uppercase)
            person["email"] = password_reset_mail
            person['is_admin'] = False
            callback(person)
            if create_admin_accounts:
                person['username'] = self.__get_username(person["firstname"], person["lastname"], admin=True)
                person['is_admin'] = True
                callback(person)

    def __get_firstname_and_gender(self):
        to_split_result = self.__get_random_list_entry("firstname")
        return to_split_result.split('\t')

    def __get_random_list_entry(self, category):
        if not category in self.lists:
            with open(self.input_files_list[category], encoding="utf-8") as f:
                lines_with_comments = f.read().splitlines()
            self.lists[category] = [entry for entry in lines_with_comments if not entry.startswith('#')]
        return random.choice(self.lists[category])

    def __get_username(self, firstname, lastname, admin=False):
        if self.randomize_username:
            username = unicodedata.normalize('NFKD', firstname+"."+lastname).encode('ascii', 'ignore')
            if admin:
                return username.decode().lower()+"-admin"
            else:
                return username.decode().lower()
        else:
            if admin:
                return 'admin.'+str(self.usercounter)
            else:
                return 'user.'+str(self.usercounter)

    def __get_title(self, gender = 'f'):
        gen_title = 'Frau' if gender == 'f' else 'Herr'
        titles = [ gen_title ] * 20
        titles.extend([ '', 'Dr.', 'Prof.'])
        return random.choice(titles)

    def __get_image(self, gender):
        if (not hasattr(self, "input_filelist_img_dict")):
            self.input_filelist_img_dict = {}
        if (not gender in self.input_filelist_img_dict):
            self.input_filelist_img_dict[gender] = glob.glob(self.input_dir_imgs_base+gender+'/*.jpg')

        selected_image = random.choice(self.input_filelist_img_dict[gender])
        with open(selected_image, "rb") as image_file:
            return base64.b64encode(image_file.read()).decode()
